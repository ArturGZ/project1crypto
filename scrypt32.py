from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives import hashes
import secrets
import string
import time
import os
import statistics
import matplotlib.pyplot as plt
import numpy as np


# Define the input vector (password) and salt (salt)
def generationVector(vector_size, len_pass):
    passwords = []
    salts = []
    for _ in range (vector_size):
        # Generate a 16-byte random salt
        salt = os.urandom(16)
        #len_pass = random.randint(20, 128)
        # Generate a random bit string
        random_data = ''.join(secrets.choice(string.ascii_letters + string.digits) for _ in range(len_pass))
        # print(type(random_data))
        data = bytes(random_data, 'utf-8')
        # print(data)
        passwords.append(data)
        salts.append(salt)
    return passwords, salts


def scrypt(passwords, salts):
    scrypt_execution_time = []
    # Configure Scrypt algorithm parameters
    scrypt_iterations = 100000  # Number of iterations
    scrypt_output_length = 32  # Output length (32 bytes = 256 bits)
    for password,salt in zip(passwords, salts):
        start_time = time.time()
        # Create the Scrypt object
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            salt=salt,
            iterations=scrypt_iterations,
            length=scrypt_output_length
        )

        # Derive a key from password and salt
        key = kdf.derive(password)
        # Print the derived key
        # print("Derived key:", key.hex())
        end_time = time.time()
        scrypt_execution_time.append(end_time - start_time)                     ###

    return scrypt_execution_time


# Calculate mean and variance of execution times
def stats(execution_times):
    mean = statistics.mean(execution_times)
    variance = statistics.variance(execution_times)
    return mean, variance


def main_scrypt():
    vector_size = 100
    len_pass = 256
    passwords, salts = generationVector(vector_size, len_pass)
    scrypt_execution_time = scrypt(passwords, salts)

    # Getting statistics
    mean_scrypt_derive, variance_scrypt_derive = stats(scrypt_execution_time)

    # Results:
    print('Vector size: ', vector_size)
    print('Size data: ', len_pass*8)
    print(f'\nMean Scrypt derive: {mean_scrypt_derive:.6f}')
    print(f'Variance Scrypt derive: {variance_scrypt_derive:.12f}')

    ####################
    # Graphing results #
    ####################
    # Data from graph 1 (Scrypt)
    algorithms_1 = ['Scrypt Output size 32 bytes']

    # Create the figure and axes
    fig, ax = plt.subplots()

    bar_width = 0.3

    # Create the bar graph for Scrytp 32
    ax.bar(algorithms_1, mean_scrypt_derive, bar_width, label='Average time')
    ax.set_xlabel(f'Length password {str(len_pass)}')
    ax.set_ylabel('Average time (s)')
    ax.set_title('Average time of Scrypt')
    ax.set_xticks(algorithms_1)
    # ax.set_xticklabels(algorithms_1)
    ax.legend()
    ax.grid()   # Show a background grid

    # Show a background grid
    ax.grid()

    # Adjuust the spacing between subgraphs
    plt.tight_layout()

    # Show the graph
    plt.show()

main_scrypt()