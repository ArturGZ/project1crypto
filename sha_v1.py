import hashlib
import secrets
import string
import time
import statistics
import matplotlib.pyplot as plt
import numpy as np

# Número de vectores de prueba
num_vectors = 100
num_executions = 100  # Realiza 100 ejecuciones para obtener estadísticas

# Función para generar una cadena de bits aleatoria con longitud variable
def generate_random_bits(length):
    if length <= 0:
        raise ValueError("La longitud debe ser un entero positivo")

    num_bytes, extra_bits = divmod(length, 8)

    if extra_bits > 0:
        num_bytes += 1

    random_bytes = secrets.token_bytes(num_bytes)
    bit_string = bin(int.from_bytes(random_bytes, byteorder='big'))[2:]

    # Añadir ceros a la izquierda para obtener la longitud correcta
    if len(bit_string) < length:
        bit_string = '0' * (length - len(bit_string)) + bit_string

    return bit_string

# Listas para almacenar los tiempos de ejecución
execution_times_sha2 = []
execution_times_sha3 = []

# Generar un valor de prueba aleatorio entre 8 y 600 bits 
length = secrets.randbelow(600) + 1

# Crear una función para realizar las pruebas
def perform_test_sha2():
    total_time = 0
    for i in range(num_vectors):
        
        random_bits = generate_random_bits(length)

        sha2 = hashlib.sha512()
        start_time = time.time()
        sha2.update(random_bits.encode('utf-8'))
        end_time = time.time()
        elapsed_time = end_time - start_time
        total_time += elapsed_time
    execution_times_sha2.append(total_time)

def perform_test_sha3():
    total_time = 0
    for i in range(num_vectors):
        random_bits = generate_random_bits(length)

        sha3 = hashlib.sha3_512()
        start_time = time.time()
        sha3.update(random_bits.encode('utf-8'))
        end_time = time.time()
        elapsed_time = end_time - start_time
        total_time += elapsed_time
    execution_times_sha3.append(total_time)

# Realizar múltiples ejecuciones para SHA-2
for i in range(num_executions):
    perform_test_sha2()

# Realizar múltiples ejecuciones para SHA-3
for i in range(num_executions):
    perform_test_sha3()

# Calcular la media y la varianza de los tiempos de ejecución para SHA-2
mean_time_sha2 = statistics.mean(execution_times_sha2)
variance_sha2 = statistics.variance(execution_times_sha2)

# Calcular la media y la varianza de los tiempos de ejecución para SHA-3
mean_time_sha3 = statistics.mean(execution_times_sha3)
variance_sha3 = statistics.variance(execution_times_sha3)

print(f"Número de ejecuciones: {num_executions}")
print(f"Tamaño de los mensajes: {length} bits")
print(f"Tiempo promedio de ejecución por ejecución (SHA-2): {mean_time_sha2:.6f} segundos")
print(f"Varianza de los tiempos de ejecución (SHA-2): {variance_sha2:.16f} segundos^2")
print(f"Tiempo promedio de ejecución por ejecución (SHA-3): {mean_time_sha3:.6f} segundos")
print(f"Varianza de los tiempos de ejecución (SHA-3): {variance_sha3:.16f} segundos^2")


# Graficar los resultados
algorithms = ['SHA-2', 'SHA-3']
num_algorithms = len(algorithms)
mean_times = [mean_time_sha2, mean_time_sha3]

# Ancho de las barras
bar_width = 0.35

# Coordenadas para la ubicación de las barras
index = np.arange(num_algorithms)

# Crear la gráfica de barras
plt.bar(index, mean_times, bar_width, label='Tiempo Promedio')

# Etiquetas y título
plt.xlabel('Algoritmo')
plt.ylabel('Tiempo Promedio (segundos)')
plt.title('Tiempo Promedio de Ejecución de SHA-2 y SHA-3')
plt.xticks(index, algorithms)
plt.legend()

# Mostrar la gráfica
plt.show()