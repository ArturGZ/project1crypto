# It is this code that implements the algorithms
# RSA-OAEP 2048 bits
# RSA-PSS 2048 bits
# The comparison will be for the operations: Signing and Verifying (They should also compare with Elliptic curves)

# Library cryptography
import cryptography
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
import secrets
import string
import time
import statistics
import matplotlib.pyplot as plt
import numpy as np


# Generation keys for RSA 
private_key = rsa.generate_private_key(
    public_exponent=65537,
    key_size=2048
)
public_key = private_key.public_key()

def generationVector(vector_size, data_length):
    # Data test
    # Data length max 190
    messages = []
    for _ in range (vector_size):
        # Generate a random bit string
        random_data = ''.join(secrets.choice(string.ascii_letters + string.digits) for _ in range(data_length))
        # print(type(random_data))
        data = bytes(random_data, 'utf-8')
        # print(data)
        messages.append(data)
    return messages


# Sign using RSA-PSS
def signRSA_PSS(messages):
    signed_data_pss = []
    signed_execution_time_rsa_pss = []
    for message in messages:
        start_time = time.time()
        signature = private_key.sign(
            message,
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH
            ),
            hashes.SHA256()
        )
        signed_data_pss.append(signature)
        end_time = time.time()
        signed_execution_time_rsa_pss.append(end_time - start_time)                    ###

    return signed_data_pss, signed_execution_time_rsa_pss



# Encryption using RSA-OAEP
def encryptionRSA_OAEP(messages):
    encrypted_data_oaep = []
    encrypted_execution_time_rsa_oaep = []
    for message in messages:
        start_time = time.time()
        try:
            ciphertext = public_key.encrypt(
                message,
                padding.OAEP(
                    mgf=padding.MGF1(algorithm=hashes.SHA256()),
                    algorithm=hashes.SHA256(),
                    label=None
                )
            )
            encrypted_data_oaep.append(ciphertext)
        except:
            print('An error occurred: Encryption failed \n')
            break
        end_time = time.time()
        encrypted_execution_time_rsa_oaep.append(end_time - start_time)                ###

    return encrypted_data_oaep, encrypted_execution_time_rsa_oaep



# Decryption using RSA-OAEP
def decryptionRSA_OAEP(encrypted_data_oaep):
    decrypted_execution_time = [] 
    for ciphertext in encrypted_data_oaep:
        start_time = time.time()
        try:
            decrypted_message = private_key.decrypt(
                ciphertext,
                padding.OAEP(
                    mgf=padding.MGF1(algorithm=hashes.SHA256()),
                    algorithm=hashes.SHA256(),
                    label=None
                )
            )
        except:
            print('An error occurred: Decryption failed \n')
            break
        end_time = time.time()
        decrypted_execution_time.append(end_time - start_time)                     ###

    return decrypted_execution_time



# Verified signing  RSA-PSS
def verifying(messages, signed_data_pss):
    verifying_execution_time = []
    for message, signature in zip(messages, signed_data_pss):
        start_time = time.time()
        try:
            public_key.verify(
                signature,
                message,
                padding.PSS(
                    mgf=padding.MGF1(hashes.SHA256()),
                    salt_length=padding.PSS.MAX_LENGTH
                ),
                hashes.SHA256()
            )
        except cryptography.exceptions.InvalidSignature:
            print("Signature verification error")
            break
        end_time = time.time()
        verifying_execution_time.append(end_time - start_time)                         ###
    
    return verifying_execution_time



# Calculate mean and variance of execution times
def stats(execution_times):
    mean = statistics.mean(execution_times)
    variance = statistics.variance(execution_times)
    return mean, variance


def main_rsa():
    vector_size = 1000
    data_length = 190   # Max 190
    messages=generationVector(vector_size, data_length)
    signed_data_pss, signed_execution_time_rsa_pss = signRSA_PSS(messages)

    encrypted_data_oaep, encrypted_execution_time_rsa_oaep = encryptionRSA_OAEP(messages)
    decrypted_execution_time_rsa_oaep = decryptionRSA_OAEP(encrypted_data_oaep)
    verifying_execution_time_rsa_pss = verifying(messages, signed_data_pss)

    # Getting statistics
    mean_encry_rsa_aoep, variance_encry_rsa_aoep = stats(encrypted_execution_time_rsa_oaep)
    mean_decry_rsa_aoep, variance_decry_rsa_aoep = stats(decrypted_execution_time_rsa_oaep)

    mean_signed_rsa_pss, variance_signed_rsa_pss = stats(signed_execution_time_rsa_pss)
    mean_verifying_rsa_pss, variance_verifying_rsa_pss = stats(verifying_execution_time_rsa_pss)


    # Results:
    print('Vector size: ', vector_size)
    print('Size data: ', data_length*8)
    print(f'\nMean RSA-AOEP Encryption: {mean_encry_rsa_aoep:.6f}')
    print(f'Variance RSA-AOEP Encryption: {variance_encry_rsa_aoep:.12f}')
    print(f'\nMean RSA-AOEP Decryption: {mean_decry_rsa_aoep:.6f}')
    print(f'Variance RSA-AOEP Decryption: {variance_decry_rsa_aoep:.12f}')
    print(f'\nMean RSA-PSS Signing: {mean_signed_rsa_pss:.6f}')
    print(f'Variance RSA-PSS Signing: {variance_signed_rsa_pss:.12f}')
    print(f'\nMean RSA-PSS Verifying: {mean_verifying_rsa_pss:.6f}')
    print(f'Variance RSA-PSS Verifying: {variance_verifying_rsa_pss:.12f}')
    

    ####################
    # Graphing results #
    ####################
    # Data from graph 1 (RSA-AOEP)
    algorithms_1 = ['RSA-AOEP Encryption', 'RSA-AOEP Decryption']
    num_algorithms_1 = len(algorithms_1)
    mean_times_1 = [mean_encry_rsa_aoep, mean_decry_rsa_aoep]

    # Data from graph 2 (RSA-PSS)
    algorithms_2 = ['RSA-PSS Signing', 'RSA-PSS Verifying']
    num_algorithms_2 = len(algorithms_2)
    mean_times_2 = [mean_signed_rsa_pss, mean_verifying_rsa_pss]

    # Bar width
    bar_width = 0.2

    # Coordinates for the location of the bars
    index = np.arange(max(num_algorithms_1, num_algorithms_2))

    # Create a figure with two subplots
    fig, axs = plt.subplots(1, 2, figsize=(12, 5))  # One row, two columns

    # Create the bar graph for RSA-AOEP
    axs[0].bar(index[:num_algorithms_1], mean_times_1, bar_width, label='Average time')
    axs[0].set_xlabel(f'Data size {str(data_length)}')
    axs[0].set_ylabel('Average time (s)')
    axs[0].set_title('Average time of RSA-AOEP')
    axs[0].set_xticks(index[:num_algorithms_1])
    axs[0].set_xticklabels(algorithms_1)
    axs[0].legend()
    # axs[0].grid()   # Show a background grid

    # Create the bar graph for RSA-PSS
    axs[1].bar(index[:num_algorithms_2] + bar_width, mean_times_2, bar_width, label='Average time')
    axs[1].set_xlabel(f'Data size {str(data_length)}')
    axs[1].set_ylabel('Average time (s)')
    axs[1].set_title('Average time of RSA-PSS')
    axs[1].set_xticks(index[:num_algorithms_2] + bar_width)
    axs[1].set_xticklabels(algorithms_2)
    axs[1].legend()
    # axs[1].grid()   # Show a background grid

    # Adjuust the spacing between subgraphs
    plt.tight_layout()

    # Show the graph
    plt.show()


main_rsa()