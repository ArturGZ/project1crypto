#ECDSA

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.asymmetric import ed25519
import time 
import statistics
import numpy as np 
import matplotlib.pyplot as plt 
import secrets
import string

def generationVector(vector_size, data_length, message_lengths):
    # Data test
    # Data length max 190
    messages = []
    for _ in range (vector_size):
        # Generate a random bit string
        random_data = ''.join(secrets.choice(string.ascii_letters + string.digits) for _ in range(data_length))
        # print(type(random_data))
        data = bytes(random_data, 'utf-8')
        # print(data)
        messages.append(data)
        message_lengths.append(data_length)
        
    return messages

def stats(execution_times):
    mean = statistics.mean(execution_times)
    variance = statistics.variance(execution_times)
    return mean, variance

def generar_llaves_ECDSA():
    llave_privada = ec.generate_private_key(ec.SECP521R1(), default_backend())
    llave_publica = llave_privada.public_key()
    return llave_privada, llave_publica

def firmar_mensaje_ECDSA(llave_privada, mensajes, execution_times_ecdsa_sign, firmas):
    
    for mensajeF in mensajes:
        start_time = time.time()
        firma = llave_privada.sign(
            mensajeF,
            ec.ECDSA(hashes.SHA256())
        )
        end_time = time.time()
        total_time = end_time - start_time
        execution_times_ecdsa_sign.append(total_time)
        firmas.append(firma)

def verificar_firma_ECDSA(llavePublica, mensajes, execution_times, firmas):
    for mensajeVF, firma in zip(mensajes, firmas):
        try:
            start_time = time.time()
            llavePublica.verify(
                firma,
                mensajeVF,
                ec.ECDSA(hashes.SHA256())
            )
            end_time = time.time()
            total_time = end_time - start_time
            execution_times.append(total_time)
        except:
            return False
    return True

def generar_llaves_EDDSA():
    llavePrivada = ed25519.Ed25519PrivateKey.generate()
    llavePublica = llavePrivada.public_key()
    return llavePrivada, llavePublica

def firmar_mensaje_EDDSA(privada, mensajes, execution_times, firmas):
    
    for mensajeF in mensajes:
        start_time = time.time()
        firma = privada.sign(mensajeF)
        end_time = time.time()
        total_time = end_time - start_time
        execution_times.append(total_time)
        firmas.append(firma)

def verificar_firma_EDDSA(publica, mensajes, execution_times, firmas):
    for mensajeVF, firma in zip(mensajes, firmas):
        try:
            start_time = time.time()
            publica.verify(firma, mensajeVF)
            end_time = time.time()
            total_time = end_time - start_time
            execution_times.append(total_time)
        except:
            return False
    return True



# Prueba
execution_times_ecdsa_sign = []
execution_times_ecdsa_verify = []
execution_times_eddsa_sign = []
execution_times_eddsa_verify = [] 
message_lengths = []
llave_privada, llave_publica = generar_llaves_ECDSA()
llavePrivada, llavePublica = generar_llaves_EDDSA()
num_executions = 100

for i in range (num_executions):
    firmas_ECDSA = []
    firmas_EDDSA = []
    message_length = secrets.randbelow(600) + 1
    messages = generationVector(100, message_length, message_lengths)
    firma_ECDSA = firmar_mensaje_ECDSA(llave_privada, messages, execution_times_ecdsa_sign, firmas_ECDSA)
    #print("Firma ECDSA:", firma_ECDSA)
    esValido_ECDSA = verificar_firma_ECDSA(llave_publica, messages, execution_times_ecdsa_verify, firmas_ECDSA)
    #print("¿Es valida la firma?", esValido_ECDSA)
    firma_EDDSA = firmar_mensaje_EDDSA(llavePrivada, messages, execution_times_eddsa_sign, firmas_EDDSA)
    #print("Firma EdDSA:", firma_EDDSA)
    esValido_EDDSA = verificar_firma_EDDSA(llavePublica, messages, execution_times_eddsa_verify, firmas_EDDSA)
    #print("¿Es valida la firma?", esValido_EDDSA)

mean_sign_ecdsa, variance_sign_ecdsa = stats(execution_times_ecdsa_sign)
mean_verify_ecdsa, variance_verify_ecdsa = stats(execution_times_ecdsa_verify)
mean_sign_eddsa, variance_sign_eddsa = stats(execution_times_eddsa_sign)
mean_verify_eddsa, variance_verify_eddsa = stats(execution_times_eddsa_verify)

print("sign ecdsa mean: ", mean_sign_ecdsa)
print("verify ecdsa mean: ", mean_verify_ecdsa)
print("sign eddsa mean: ", mean_sign_eddsa)
print("verify eddsa mean: ", mean_verify_eddsa)

algorithms = ['ECDSA Sign', 'ECDSA Verify', 'EDDSA Sign', 'EDDSA Verify']
num_algorithms = len(algorithms)
mean_times = [mean_sign_ecdsa, mean_verify_ecdsa, mean_sign_eddsa, mean_verify_eddsa]

# Ancho de las barras
bar_width = 0.35

# Coordenadas para la ubicación de las barras
index = np.arange(num_algorithms)
plt.figure(figsize=(8, 6))
plt.subplot(3,3,1)
# Crear la gráfica de barras
plt.bar(index, mean_times, bar_width, label='Tiempo Promedio')

# Etiquetas y título
plt.xlabel('Algoritmo')
plt.ylabel('Tiempo Promedio (segundos)')
plt.title('Tiempo Promedio de Ejecución ECDSA y EDDSA')
plt.xticks(index, algorithms)
plt.legend()

plt.subplot(3,3,2)
plt.scatter(message_lengths, execution_times_ecdsa_sign)
plt.xlabel("Datos")
plt.ylabel("Tiempo")
plt.title("Firma ECDSA")

plt.subplot(3,3,3)
plt.scatter(message_lengths, execution_times_ecdsa_verify)
plt.xlabel("Datos")
plt.ylabel("Tiempo")
plt.title("Verificacion ECDSA")

plt.subplot(3,3,4)
plt.scatter(message_lengths, execution_times_eddsa_sign)
plt.xlabel("Datos")
plt.ylabel("Tiempo")
plt.title("Firma EDDSA")

plt.subplot(3,3,5)
plt.scatter(message_lengths, execution_times_eddsa_verify)
plt.xlabel("Datos")
plt.ylabel("Tiempo")
plt.title("Verificacion EDDSA")

plt.show()
