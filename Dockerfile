FROM alpine:3.14

RUN apk --no-cache add \
    python3 \
    py3-pip \
    py3-cryptography \
    py3-matplotlib \
    py3-pycryptodome \
    xauth \
    xorg-server

RUN adduser -D -h /my-app appuser

WORKDIR /my-app

ENV DISPLAY=:0

COPY . /my-app
RUN chmod +x entrypoint.sh

USER appuser 
 
# CMD ["python3","rsa.py"]
ENTRYPOINT [ "./entrypoint.sh" ]