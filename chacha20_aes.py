import secrets
import string
import time
import statistics
import matplotlib.pyplot as plt
import numpy as np
from Crypto.Cipher import ChaCha20
from Crypto.Cipher import AES
import json
from base64 import b64encode
from Crypto.Random import get_random_bytes
from Crypto.Util.Padding import pad, unpad
#Encriptar

def generationVector(vector_size, data_length, message_lengths):
    # Data test
    # Data length max 190
    messages = []
    for _ in range (vector_size):
        # Generate a random bit string
        random_data = ''.join(secrets.choice(string.ascii_letters + string.digits) for _ in range(data_length))
        # print(type(random_data))
        data = bytes(random_data, 'utf-8')
        # print(data)
        messages.append(data)
        message_lengths.append(data_length)
        
    return messages

def stats(execution_times):
    mean = statistics.mean(execution_times)
    variance = statistics.variance(execution_times)
    return mean, variance

def AES_ECB(messages, cipherMessages, execution_times_AES_ECB_decrypt, execution_times_AES_ECB_encrypt, key):
    cipher = AES.new(key, AES.MODE_ECB)
    total_time_encrypt = 0
    block_size = 32
    for message in messages:
        start_time = time.time()
        cipherText = cipher.encrypt(pad(message, block_size))
        end_time = time.time()
        total_time_encrypt = end_time - start_time
        cipherMessages.append(cipherText)
        execution_times_AES_ECB_encrypt.append(total_time_encrypt)
    decipher = AES.new(key, AES.MODE_ECB)
    total_time_decrypt = 0
    for cipherMessage in cipherMessages:
        start_time = time.time()
        plaintText = decipher.decrypt(cipherMessage)
        end_time = time.time()
        total_time_decrypt = end_time - start_time
        execution_times_AES_ECB_decrypt.append(total_time_decrypt)

def AES_GCM(messages, cipherMessages, execution_times_AES_GCM_decrypt, execution_times_AES_GCM_encrypt, tags, key):
    total_time_encrypt = 0
    count = 0
    #print("llave de cifrado: ", key)
    for message in messages:
        start_time = time.time()
        cipher = AES.new(key, AES.MODE_GCM)
        cipherText, tag = cipher.encrypt_and_digest(message)
        #if(count < 1):
        #    print("For_text: ", cipherText)
        #    print("For_tag: ", tag)
        #    count += 1
        end_time = time.time()
        total_time_encrypt = end_time - start_time
        cipherMessages.append(cipherText)
        tags.append(tag)
        execution_times_AES_GCM_encrypt.append(total_time_encrypt)
# Para descifrar, necesitas la clave y el tag
    nonce = get_random_bytes(12) 
    total_time_decrypt = 0 
    #print("llave decifrado: ", key)
    #print("text: ", cipherText[0])
    #print("tags: ", tags[0])
    for i in range (len(cipherMessages)):
        #print("\ntext: ", cipherMessages[i])
        #print("\ntag: ", tags[i])
        start_time = time.time()
        cipher = AES.new(key, AES.MODE_GCM, nonce=nonce)
        mensaje_descifrado = cipher.decrypt(cipherMessages[i])
        #mensaje_verificado = cipher.verify(tags[i])
        end_time = time.time()
        total_time_decrypt = end_time - start_time
        execution_times_AES_GCM_decrypt.append(total_time_decrypt)


    # Crear una función para realizar las pruebas
def perform_test_chacha20(messages, cipherMessages, execution_times_chacha20_decrypt, execution_times_chacha20_encrypt, key):

    nonce = get_random_bytes(8)
    total_time_encrypt = 0
    cipher = ChaCha20.new(key=key)
    #print("parte1: ", len(execution_times_chacha20_decrypt))
    for message in messages:
        start_time = time.time()
        cipherText = cipher.encrypt(message)
        end_time = time.time()
        total_time_encrypt = end_time - start_time
        cipherMessages.append(cipherText)
        execution_times_chacha20_encrypt.append(total_time_encrypt)
    total_time_decrypt = 0
    cipher = ChaCha20.new(key=key, nonce=nonce)
    #print(len(cipherMessages))
    for cipherMessage in cipherMessages:
        start_time = time.time()
        finalText = cipher.decrypt(cipherMessage)
        end_time = time.time()
        total_time_decrypt = end_time - start_time
        execution_times_chacha20_decrypt.append(total_time_decrypt)
    #print("parte2: ", len(execution_times_chacha20_decrypt))
    #print("cipherMessage: ", len(cipherMessages))

execution_times_chacha20_encrypt = []
execution_times_chacha20_decrypt = []
execution_times_ecb_encrypt = []
execution_times_ecb_decrypt = []
execution_times_gcm_encrypt = []
execution_times_gcm_decrypt = [] 
message_lengths = []
key = get_random_bytes(32)
num_executions = 100

for i in range(num_executions):
    cipherMessages = []
    cipherMessages_ecb = []
    cipherMessages_gcm = []
    tags = []
    message_length = secrets.randbelow(600) + 1
    messages = generationVector(100, message_length, message_lengths)
    AES_ECB(messages, cipherMessages_ecb, execution_times_ecb_decrypt, execution_times_ecb_encrypt, key)
    AES_GCM(messages, cipherMessages_gcm, execution_times_gcm_decrypt, execution_times_gcm_encrypt, tags, key)
    perform_test_chacha20(messages, cipherMessages, execution_times_chacha20_decrypt, execution_times_chacha20_encrypt, key)

mean_encry_chacha20, variance_encry_chacha20 = stats(execution_times_chacha20_encrypt)
mean_decry_chacha20, variance_decry_chacha20 = stats(execution_times_chacha20_decrypt)
mean_encry_ecb, variance_encry_ecb = stats(execution_times_ecb_encrypt)
mean_decry_ecb, variance_decry_ecb = stats(execution_times_ecb_decrypt)
mean_encry_gcm, variance_encry_gcm = stats(execution_times_gcm_encrypt)
mean_decry_gcm, variance_decry_gcm = stats(execution_times_gcm_decrypt)


#print(len(execution_times_chacha20_decrypt))
#print(len(message_lengths))
# Graficar los resultados
algorithms = ['ChaCha20 Encrypt', 'ChaCha20 Decrypt', 'AES_ECB Encrypt', 'AES_ECB Decrypt', 'AES_GCM Encrypt', 'AES_GCM Decrypt']
num_algorithms = len(algorithms)
mean_times = [mean_encry_chacha20, mean_decry_chacha20, mean_encry_ecb, mean_decry_ecb, mean_encry_gcm, mean_decry_gcm]

# Ancho de las barras
bar_width = 0.35

# Coordenadas para la ubicación de las barras
index = np.arange(num_algorithms)

plt.subplot(3,3,1)
# Crear la gráfica de barras
plt.bar(index, mean_times, bar_width, label='Tiempo Promedio')

# Etiquetas y título
plt.xlabel('Algoritmo')
plt.ylabel('Tiempo Promedio (segundos)')
plt.title('Tiempo Promedio de Ejecución ChaCha20 y AES')
plt.xticks(index, algorithms)
plt.legend()

plt.subplot(3,3,2)
plt.scatter(message_lengths, execution_times_chacha20_encrypt)
plt.xlabel("Datos")
plt.ylabel("Tiempo")
plt.title("Encriptado ChaCha20")

plt.subplot(3,3,3)
plt.scatter(message_lengths, execution_times_chacha20_decrypt)
plt.xlabel("Datos")
plt.ylabel("Tiempo")
plt.title("Desencriptado ChaCha20")

plt.subplot(3,3,4)
plt.scatter(message_lengths, execution_times_ecb_encrypt)
plt.xlabel("Datos")
plt.ylabel("Tiempo")
plt.title("Encriptado AES-ECB")

plt.subplot(3,3,5)
plt.scatter(message_lengths, execution_times_ecb_decrypt)
plt.xlabel("Datos")
plt.ylabel("Tiempo")
plt.title("Desencriptado AES-ECB")

plt.subplot(3,3,6)
plt.scatter(message_lengths, execution_times_gcm_encrypt)
plt.xlabel("Datos")
plt.ylabel("Tiempo")
plt.title("Encriptado AES-GCM")

plt.subplot(3,3,7)
plt.scatter(message_lengths, execution_times_gcm_decrypt)
plt.xlabel("Datos")
plt.ylabel("Tiempo")
plt.title("Desencriptado AES-GCM")

# Mostrar la gráfica
plt.show()
